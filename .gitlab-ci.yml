stages:
  - build
  - test
  - dockerize
  - push
  - development
  - production

variables:
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  DOCKER_CONTEXT: "."
  IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE:latest
  IMAGE_TAG_BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  IMAGE_TAG_PIPELINE_NUMBER: $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID
  IMAGE_TAG_SLUG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  IMAGE_TAG_COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

# Cache modules in between jobs
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository

.docker_template: &docker_anchor
  image: docker:latest
  services:
    - docker:dind
  tags:
    - docker

maven-build:
  <<: *docker_anchor
  stage: build
  image: maven:3-jdk-8-alpine
  before_script:
    - mvn --version
  script: 
    - mvn clean install
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 hour

maven-test:
  <<: *docker_anchor
  stage: test
  image: maven:3-jdk-8-alpine
  needs:
    - job: maven-build
      artifacts: true
  script:
    # - mvn test
    - 'mvn $MAVEN_CLI_OPTS verify'
  except:
    - master

docker-image-build:
  <<: *docker_anchor
  stage: dockerize
  when: on_success
  before_script:
    - docker --version
    - docker info
    - echo -n $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    # fetches the latest image (not failing if image is not found)
    - docker pull $IMAGE_TAG_LATEST || true
  script:
    - docker build --pull --build-arg VCS_REF=$CI_COMMIT_SHA --build-arg VCS_URL=$CI_PROJECT_URL --cache-from $IMAGE_TAG_LATEST -t $IMAGE_TAG_COMMIT $DOCKER_CONTEXT
    - docker tag $IMAGE_TAG_COMMIT $IMAGE_TAG_PIPELINE_NUMBER
    - docker tag $IMAGE_TAG_COMMIT $IMAGE_TAG_SLUG
    - docker push -a $CI_REGISTRY_IMAGE

docker-push-latest:
  <<: *docker_anchor
  variables:
    # We are just playing with Docker here. 
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  only:
    # Only "master" should be tagged "latest"
    - master
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $IMAGE_TAG_COMMIT
    # Then we tag it "latest"
    - docker tag $IMAGE_TAG_COMMIT $IMAGE_TAG_LATEST
    # Annnd we push it.
    - docker push $IMAGE_TAG_LATEST

docker-push-tag:
  <<: *docker_anchor
  variables:
    # Again, we do not need the source code here. Just playing with Docker.
    GIT_STRATEGY: none
  stage: push
  only:
    # We want this job to be run on tags only.
    - tags
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker pull $IMAGE_TAG_COMMIT
    - docker tag $IMAGE_TAG_COMMIT $IMAGE_TAG_BRANCH
    - docker push $IMAGE_TAG_BRANCH

deploy-dev:
  <<: *docker_anchor
  stage: development
  image: 
    name: alpine/git:v2.30.1
    entrypoint: [""]
  script:
    - echo dev
    - git --version
  environment:
    name: dev

deploy-prod:
  <<: *docker_anchor
  stage: production
  image: 
    name: alpine/git:v2.30.1
    entrypoint: [""]
  script:
    - echo prod
  environment:
    name: prod
  dependencies:
    - deploy-dev

